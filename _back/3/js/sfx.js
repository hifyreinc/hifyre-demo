try {
	parent.document.getElementById('sceneNav').style.display='none';
	top.disableScreenLock(true);
	window.parent.setDisableScroll(false);
} catch(e){

}

$(function(){
	
	/*
$(document).on('touchmove', function(event){ 
        if (event.target.tagName != "VIDEO") { 
            event.preventDefault();
        }
    });
*/
	
	$('body').on('mouseup', '.btn-return-to-library', function(){
		try {
			return_to_library();
		} catch(e){
			try {
				top.cancelPresentation(false);
			} catch(e){
				alert('Exit Presentation');
			}
		}
	});
	
});

function goWeb(link){
	try {
		Ti.App.fireEvent('launchWebview', { title: "", url: link, desc: "" });
	} catch(e){
		window.open(link,'_blank');
	}
	analytics('Link: '+link);
}
