var swiperCustomcallsList;

$(function(){
	
	$('body').hammer().on('tap', '.btn-add-call', function(e){
		e.gesture.stopDetect();
		swal({   
			title: "",   
			text: "Enter a name for your Custom Call",   
			type: "input",   
			showCancelButton: true,   
			closeOnConfirm: false,   
			animation: "slide-from-top",   
			inputPlaceholder: "Write something" 
		}, function(inputValue){   
			if (inputValue === false) 
				return false;      
			if (inputValue === "") {     
				swal.showInputError("You need to write something!");     
				return false   
			}
			swal.close();
			//swal("Nice!", "You wrote: " + inputValue, "success"); });
			var newCall = [];
			newCall[0] = new Date().getTime();
			newCall[1] = inputValue;
			newCall[2] = new Array();
			
			customCallsArray.push(newCall);
			localStorage[prefix+'custom_calls'] = JSON.stringify(customCallsArray);
			
			$('.all-calls').append('<div class="btn btn-default" data-gocall="'+customCallsArray.length+'">'+newCall[1]+'</div>');
			
			swiperCustomcallsList.refresh();
			
			$('[data-gocall="'+customCallsArray.length+'"]').trigger('tap');
			$('.btn-edit').trigger('tap');
			swiperCustomcallsList.scrollToElement('[data-gocall].active');
		});
		
		
	});
	
	$('body').hammer().on('tap', '.btn-rename', function(e){
		e.gesture.stopDetect();
		swal({   
			title: "",   
			text: "Rename your Custom Call",   
			type: "input",   
			showCancelButton: true,   
			closeOnConfirm: false,   
			animation: "slide-from-top",   
			inputPlaceholder: "Write something" 
		}, function(inputValue){   
			if (inputValue === false) 
				return false;      
			if (inputValue === "") {     
				swal.showInputError("You need to write something!");     
				return false   
			}  
			swal.close();    
			//swal("Nice!", "You wrote: " + inputValue, "success"); });
			var callidnum = $('[data-gocall].active').attr('data-gocall');
			var thisCall = customCallsArray[callidnum-1];
			thisCall[1] = inputValue;
			
			customCallsArray[callidnum-1] = thisCall;
			localStorage[prefix+'custom_calls'] = JSON.stringify(customCallsArray);
			
			$('[data-gocall="'+callidnum+'"]').html(inputValue);
			
			swiperCustomcallsList.refresh();
			
			$('[data-gocall="'+callidnum+'"]').trigger('tap');
		});
	});
	
	$('body').hammer().on('tap', '[data-gocall]', function(){
		displayCustomCall($(this).attr('data-gocall'));
	});
	
	$('body').hammer().on('tap', '.btn-edit', function(){
		//$('.hidden-on-edit .slide-thumb').removeClass('chosen');
		if($('.slide-thumb.chosen').length == 0){
			$('.slide-thumb').each(function(){
				//$(this).addClass('selected').attr('data-slideorder',$(this).attr('data-originalslideorder')).find('.slide-order span').html($(this).attr('data-originalslideorder')*1+1);
				$(this).removeClass('selected').find('.slide-order span').html('');
			});
		} else {
			$('.slide-thumb.chosen').each(function(i,v){
				$(this).addClass('selected').attr('data-slideorder',i).find('.slide-order span').html(i+1);
			});
		}
		
		var $wrapper = $('#slidelibrary-container > .row');
		$wrapper.find('.col-xs-3').sort(function (a, b) {
		    return +$('.slide-thumb', a).attr('data-originalslideorder') - +$('.slide-thumb', b).attr('data-originalslideorder');
		}).appendTo( $wrapper );
		
		$('.edit-state').addClass('editing');
		$('.btn-cancel').tooltip('show');
		setTimeout(function(){
			swiperSlidelibrary.swipeTo(0,0);
			swiperSlidelibrary.reInit();
			//swiperSlidelibrary.resizeFix(true);
		}, 10);
	});
	
	
	
	$('body').hammer().on('tap', '.btn-cancel', function(){
		$('.btn-cancel').tooltip('hide');
		$('.slide-thumb').each(function(){
			$(this).removeClass('selected').find('.slide-order span').html('');
		});
		var $wrapper = $('#slidelibrary-container > .row');
		$wrapper.find('.col-xs-3').sort(function (a, b) {
		    return +$('.slide-thumb', a).attr('data-slideorder') - +$('.slide-thumb', b).attr('data-slideorder');
		}).appendTo( $wrapper );
		$('.edit-state').removeClass('editing');
		setTimeout(function(){
			swiperSlidelibrary.swipeTo(0,0);
			swiperSlidelibrary.reInit();
			//swiperSlidelibrary.resizeFix(true);
		}, 10);
	});
	
	
	
	$('body').hammer().on('tap', '.btn-save', function(){
		$('.btn-cancel').tooltip('hide');
		//$('.hidden-on-edit .slide-thumb').addClass('selected');
		if($('.slide-thumb.selected').length == 0){
			$('.slide-thumb').each(function(){
				//$(this).addClass('selected').attr('data-slideorder',$(this).attr('data-originalslideorder')).find('.slide-order span').html($(this).attr('data-originalslideorder')).closest('.col-xs-3').removeClass('unchosen');
				$(this).removeClass('selected').find('.slide-order span').html('');
			});
		}
		$('.slide-thumb:not(.selected)').removeClass('chosen').closest('.col-xs-3').addClass('unchosen');
		$('.slide-thumb.selected').addClass('chosen').closest('.col-xs-3').removeClass('unchosen');
		$('.slide-thumb').each(function(){
			$(this).removeClass('selected').find('.slide-order span').html('');
		});
		var $wrapper = $('#slidelibrary-container > .row');
		$wrapper.find('.col-xs-3').sort(function (a, b) {
		    return +$('.slide-thumb', a).attr('data-slideorder') - +$('.slide-thumb', b).attr('data-slideorder');
		}).appendTo( $wrapper );
		
		var chosenSlides = [];
		$('.slide-thumb.chosen').each(function(){
			var slideid = $(this).attr('data-load');
			chosenSlides.push(slideid);
		});
		
		var callidnum = $('[data-gocall].active').attr('data-gocall');
		var thisCall = customCallsArray[callidnum-1];
		if(chosenSlides.length){
			thisCall[2] = chosenSlides;
		}
		customCallsArray[callidnum-1] = thisCall;
		localStorage[prefix+'custom_calls'] = JSON.stringify(customCallsArray);
		if(chosenSlides.length){
			//$('.btn-present').removeClass('hidden');
		} else {
			//$('.btn-present').addClass('hidden');
		}
		$('.edit-state').removeClass('editing');
		
		setTimeout(function(){
			swiperSlidelibrary.swipeTo(0,0);
			swiperSlidelibrary.reInit();
			//swiperSlidelibrary.resizeFix(true);
		}, 10);
	});
	
	
	
	$('body').hammer().on('tap', '.editing .slide-thumb:not(.selected)', function(){
		var slidenum = $('.slide-thumb.selected').length;
		$(this).find('.slide-order span').html(slidenum+1);
		$(this).addClass('selected').removeClass('unchosen').attr('data-slideorder',slidenum);
	});
	
	$('body').hammer().on('tap', '.edit-state:not(.editing) .slide-thumb', function(e){
		var gotoslidenum = $(this).attr('data-load');
		var gotosection = $(this).attr('data-section_id');
		presentCall(gotoslidenum,gotosection);
	});
	
	
	
	$('body').hammer().on('tap', '.editing .slide-thumb.selected', function(){
		var originalslideorder = $(this).attr('data-originalslideorder');
		var selectedslideorder = $(this).attr('data-slideorder');
		var startorder = (selectedslideorder*1);
		
		var slidenum = $('.slide-thumb.selected').length;
		
		if(slidenum > 0){
			for(var i = startorder; i < slidenum; i++){
								
				var slide = $('[data-slideorder="'+i+'"]');
				var neworder = slide.attr('data-slideorder');
				slide.attr('data-slideorder',(neworder*1)-1).find('.slide-order span').html(neworder);
					
			}
		}
		
		$(this).removeClass('selected').attr('data-slideorder',originalslideorder).find('.slide-order span').html('');
		
	});
	
	
	
	$('body').hammer().on('tap', '.btn-reset', function(){
		$('.slide-thumb').each(function(){
			//var originalslideorder = $(this).attr('data-originalslideorder');
			//$(this).attr('data-slideorder',originalslideorder).addClass('selected').find('.slide-order span').html((originalslideorder*1)+1);
			$(this).removeClass('selected').find('.slide-order span').html('');
		});
	});
	
	
	$('body').hammer().on('tap', '.btn-delete', function(e){
		e.gesture.stopDetect();
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this custom call!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false 
		}, function(){
			var callidnum = $('[data-gocall].active').attr('data-gocall');
			var thisCall = callidnum-1;
			customCallsArray.splice(thisCall,1);
			localStorage[prefix+'custom_calls'] = JSON.stringify(customCallsArray);
			$('[data-gocall].active').remove();
			swiperCustomcallsList.refresh();
			//swiperCustomcallsList.resizeFix(true);
			
			$('[data-gocall]','.all-calls').each(function(){
				var thisindex = $(this).index() + 1;
				$(this).attr('data-gocall',thisindex);
			});
			displayCustomCall('0');
			swal("Deleted!", "Your custom call has been deleted.", "success"); 
		});
		
		
		
	});
	
	
	
	$('body').hammer().on('tap', '.btn-present', function(){
		presentCall();
	});
	
});

function loadCustomCalls(){
	var currentSection = 'custom';
	loadProcess = $.ajax({
		url: 'custom_calls.html',
		success: function(content) {
			$('.custom-calls').html(content);
			
			swiperSlidelibrary = $('.swiper-slidelibrary').swiper({
				mode: 'vertical',
				scrollContainer: true,
				scrollbar: {
					container :'.swiper-scrollbar-2',
					hide: false,
					draggable: false  
			    }
			});
			
			/*
swiperSlidelibrary = new IScroll('.swiper-slidelibrary', {
				scrollbars: true
			});
*/
			swiperCustomcallsList = new IScroll('.call-list-swiper', {
				scrollbars: true
			});
			//displaySlideOverview('detailaid','#slidelibrary-container .row');
			$('.custom-calls').show(0,function(){
				swiperCustomcallsList.refresh();
				//swiperCustomcallsList.resizeFix(true);
				$('.all-calls').html('');
				$.each(customCallsArray, function(ind,val){
					var indexcall = (ind*1) + 1;
					$('.all-calls').append('<div class="btn btn-default" data-gocall="'+indexcall+'">'+val[1]+'</div>');
					swiperCustomcallsList.refresh();
					//swiperCustomcallsList.resizeFix(true);
				});
				if(customBuild){
					displayCustomCall(customBuild);
				} else if(currentModule){
					displayCustomCall(currentModule);
				} else {
					displayCustomCall('0');
				}
				swiperCustomcallsList.scrollToElement('[data-gocall].active');
				
			});
		}
	});
}


function displayCustomCall(id){
	//console.log(thisCall);
	$('.btn-cancel').trigger('tap');
	$('[data-gocall]').removeClass('active');
	$('[data-gocall="'+id+'"]').addClass('active');
	$('.unchosen').removeClass('unchosen');
	$('.chosen').removeClass('chosen');
	
	//console.log(id);
	if(id == 0){
		swiperCustomcallsList.scrollTo(0,0);
		$('.edit-state').addClass('no-edit');
		$('.call-name strong').html($('[data-gocall="'+id+'"]').html());
		$('#slidelibrary-container .row').html('');
		//alert('0');
		displaySlideOverview('detailaid','#slidelibrary-container .row');
		var $wrapper = $('#slidelibrary-container > .row');
		$wrapper.addClass('invisible');
		setTimeout(function(){
		$wrapper.find('.col-xs-3').sort(function (a, b) {
		    return +$('.slide-thumb', a).attr('data-numericid') - +$('.slide-thumb', b).attr('data-numericid');
		}).appendTo( $wrapper );
		$wrapper.removeClass('invisible');
		}, 300);
		
	} else if(id.indexOf("default") >= 0){
		$('.edit-state').addClass('no-edit');
		$('.call-name strong').html($('[data-gocall="'+id+'"]').html());
		$('#slidelibrary-container .row').html('');
		var slidearray = $('[data-gocall="'+id+'"]').attr('data-callslides').split(',');
		displaySlideOverview('customcall','#slidelibrary-container .row',slidearray);
	} else {
		//console.log(customCallsArray);
		var thisCall = customCallsArray[id-1];
		$('.edit-state').removeClass('no-edit');
		//console.log(thisCall);
		$('.call-name strong').html(thisCall[1]);
		//console.log(thisCall[2]);
		if(thisCall[2]){
			$('#slidelibrary-container > .row').html('');
			//alert('custom');
			displaySlideOverview('customcall','#slidelibrary-container .row',thisCall[2]);
			//$('.btn-present').removeClass('hidden');
		} else {
			$('#slidelibrary-container .row').html('');
			alert('empty');
			displaySlideOverview('customcall','#slidelibrary-container .row','empty');
			//$('.btn-present').addClass('hidden');
			var $wrapper = $('#slidelibrary-container > .row');
			$wrapper.addClass('invisible');
			setTimeout(function(){
			$wrapper.find('.col-xs-3').sort(function (a, b) {
			    return +$('.slide-thumb', a).attr('data-originalslideorder') - +$('.slide-thumb', b).attr('data-originalslideorder');
			}).appendTo( $wrapper );
			$wrapper.removeClass('invisible');
			}, 300);
		}
	}
	setTimeout(function(){
		//swiperSlidelibrary.reInit();
		//swiperSlidelibrary.resizeFix(true);
	}, 10);
	
}

function presentCall(firstslide,firstsection){
	var customCallActive = $('[data-gocall].active').attr('data-gocall');
	//alert(customCallActive);
	
	//alert(customCallActive);
	if(customCallActive == 0) {
		if(!firstslide){
			firstslide = $('.slide-thumb:visible:first').attr('data-load');
		}
		if(!firstsection){
			firstsection = $('.slide-thumb:visible:first').attr('data-section_id');
		}
		goSlide(firstsection,firstslide,true);
		customBuild = null;
		currentModule = 0;
	} else if(customCallActive.indexOf("default") >= 0){ 
		var slidearray = $('[data-gocall].active').attr('data-callslides').split(',');
		currentCustomCall = slidearray;
		if(!firstslide){
			firstslide = $('.slide-thumb:visible:first').attr('data-load');
		}
		goSlide('_customcall',firstslide,true);
		customBuild = null;
		currentModule = customCallActive;
	} else {
		var thisCall = customCallsArray[customCallActive-1];
		currentModule = null;
		customBuild = customCallActive;
		currentCustomCall = thisCall[2];
		if(!firstslide){
			firstslide = currentCustomCall[0];
		}
		if(firstslide){
			goSlide('_customcall',firstslide,true);
		} else {
			swal({ 
				type: "error",
				title: "Please add slides to your custom call.", 
				allowOutsideClick: false,
				animation: false,
				//timer: 1000, 
				showConfirmButton: true 
			});
		}
		//console.log(currentCustomCall);
		//go_section('menu',null,'_0_1_a');
		
		//displaySlides('_detailaid',);
	}
}


function closeSlideOverview(){
	if($('body').hasClass('popup-open')){
		hidePopups();
		hideVideo();
	}
	if($('.swiper-slidelibrary').length){
		try {
			swiperSlidelibrary.swipeTo(0,0);
			swiperSlidelibrary.destroy();
			swiperSlidelibrary = null;
			swiperCustomcallsList.scrollTo(0,0);
			swiperCustomcallsList.destroy();
			swiperCustomcallsList = null;
			//console.log('swiper destroyed');
		} catch(e){
			//console.log('no swiper');
		}
		setTimeout(function(){
			$('.custom-calls').html('<h1><i class="fa fa-spin fa-circle-o-notch"></i></h1>');
		},500);
	}
}