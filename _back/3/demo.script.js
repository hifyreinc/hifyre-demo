//@prepros-prepend js/jquery-2.1.4.min.js
//@prepros-prepend js/jquery.hammer-full.min.js
//@prepros-prepend js/bootstrap.min.js
//@prepros-prepend js/idangerous.swiper.min.js
//@prepros-prepend js/idangerous.swiper.scrollbar.js
//@prepros-prepend js/iscroll.js
//@prepros-prepend js/Chart.min.js
//@prepros-prepend js/spritespin.min.js
//@prepros-prepend js/jquery-ui.min.js
//@prepros-prepend js/jquery.ui.touch-punch.min.js
//@prepros-prepend js/sweetalert.min.js
//@prepros-prepend js/sfx.js
//@prepros-append customcalls.js

var projectID = 'Hifyre EN';
var prefix = 'Hifyre_EN_';
var pre_prefix = 'Hifyre_';

/* Swiper Vars */
var swiperPres;
var swiperInner;
var swiperPage;
var swiperScroll;
var swiperResources;
var favsSwiper;
var swiperSlidelibrary;

if(!localStorage[prefix+'custom_calls']){
	localStorage[prefix+'custom_calls'] = JSON.stringify(new Array());	
}
var customCallsArray = JSON.parse(localStorage[prefix+'custom_calls']);
var currentCustomCall = false;
var customBuild = false;
var currentModule = false;
var gotab = null;

var rightXLimit;
var tapping = false;


/* Local Vars */
if(!localStorage[prefix+'fav_resources']){
	localStorage[prefix+'fav_resources'] = JSON.stringify(new Array());
}

if(!localStorage[prefix+'fav_slides']){
	localStorage[prefix+'fav_slides'] = JSON.stringify(new Array());
}

if(!localStorage[prefix+'request_form']){
	localStorage[prefix+'request_form'] = JSON.stringify(new Array());	
}



/***** custom content vars *****/
var swiperScroll1 = '';
var image_swap;
var spin;
var api;
var productsplit;
var spin2;
var api2;
var viewangle = 1;
var smallSwiper;


/* Page Load */
$(function(){
	
	$('body').hammer().on('dragdown', '.swiper-pres-container', function(e){
		if($(e.gesture.target).hasClass('spritespin-canvas')){
			/*
if(e.gesture.distance > 50){
				e.gesture.stopDetect();
				if (viewangle <=2 ) {
	                $('.threesixty_images').html('');
	                viewangle++;
	                initSpin360('.product-1',false);
	            }
            }
*/
		} else {
			if(e.gesture.distance > 50){
				openTopContent();
				closeBottomContent();
			}
		}
	});
	
	$('body').hammer().on('dragup', '.swiper-pres-container', function(e){
		if($(e.gesture.target).hasClass('spritespin-canvas')){
			/*
if(e.gesture.distance > 50){
				e.gesture.stopDetect();
				if (viewangle >=2 ) {
	                $('.threesixty_images').html('');
	                viewangle--;
	                initSpin360('.product-1',false);
	            }
            }
*/
		} else {
			if(e.gesture.distance > 50){
				closeTopContent();
				openBottomContent();
				e.gesture.stopDetect();
			}
		}
	});
	
	$('body').hammer().on('dragup', '.slide-content-top .pull-tab', function(e){
		closeTopContent();
	});
	
	$('body').hammer().on('dragup', '.content-cover-1', function(e){
		if(e.gesture.distance > 50){
			closeTopContent();
		}
	});
	
	$('body').hammer().on('dragdown', '.slide-content-bottom .pull-tab', function(e){
		closeBottomContent();
	});
	
	$('body').hammer().on('dragdown', '.content-cover-2', function(e){
		if(e.gesture.distance > 50){
			closeBottomContent();
		}
	});
	
	$('body').hammer().on('dragup', '.slide-content-bottom .pull-tab', function(e){
		//e.gesture.stopDetect();
		openBottomContent(true);
		e.gesture.stopDetect();
	});
	
	$('body').hammer().on('dragup', '.content-cover-2', function(e){
		if(e.gesture.distance > 50){
			openBottomContent(true);
			e.gesture.stopDetect();
		}
	});
	
	/*
$('body').hammer().on('dragup', '.swiper-pres-container', function(e){
		if(e.gesture.distance > 50){
			openBottomContent();
		}
	});
	
	$('body').hammer().on('dragdown', '.swiper-pres-container', function(e){
		if(e.gesture.distance > 50){
			closeBottomContent();
		}
	});
*/
	
	$('body').hammer().on('tap', '.resource-container .favstar', function(){
		var favid = $(this).parent().data('favid');
		$(this).parent().toggleClass('fav-on');
		if($(this).parent().hasClass('fav-on')){
			addFavResource(favid);
		} else {
			removeFavResource(favid);
		}
		$('[data-resource_filter="favs"] .badge').html($('.resource-container .fav-on').length);
		if($('[data-resource_filter="favs"]').hasClass('active')){
			$('[data-resource_filter="favs"]').trigger('tap');
		}
	});
	
	$('body').hammer().on('tap', '.swiper-slidelibrary .favstar,.favs-swiper .favstar', function(e){
		e.gesture.stopDetect();
		var data = $(this).siblings('div').data();
		$(this).parent().toggleClass('fav-on');
		if($(this).parent().hasClass('fav-on')){
			addToFavorites(data.section_id,data.load,data.slide_title);
		} else {
			removeFromFavorites(data.section_id,data.load,data.slide_title);
		}
	});
	
	$('body').hammer().on('tap', '[data-resource_filter]', function(){
		$(this).addClass('active').siblings().removeClass('active');
		var cat = $(this).data('resource_filter');
		$('[data-resourcecat]').hide();
		if (cat == 'favs'){
			$('.fav-on').show();
		} else if (cat == 'all'){
			$('[data-resourcecat]').show();
		} else {
			$('[data-resourcecat="'+cat+'"]').show();
		}
		swiperResources.swipeTo(0,0);
		swiperResources.reInit();
		swiperResources.resizeFix(true);
	});
	
	$('body').hammer().on('tap', '[data-resource]', function(e){
		$this = $(this);
		e.stopPropagation();
		e.gesture.preventDefault();
		e.gesture.stopDetect();
		e.gesture.stopPropagation();
		$(this).trigger('mouseup');
		var resource = $(this).data('resource');
		if(resource.indexOf(',') > -1){
			$this.removeAttr('data-resource');
			$this.attr('data-doubleresource',resource);
			var resourcearray = resource.split(',');
			$.each(resourcearray, function(i,v){
				var referencenum = v.split('.')[0];
				referencenum = referencenum.substring(referencenum.lastIndexOf("_") + 1);
				$this.append('<div class="choose-resource" data-resource="'+v+'">'+referencenum+'</div>');
			});
		} else {
			var resourceTitle = $('#resource-container').find('[data-resource="'+resource+'"] p').text();
			analytics('Resource/'+resourceTitle);
			goResource(resource);
		}
	});
	
	$('body').hammer().on('tap', '[data-gopres_lang]', function(){
		var pres = $(this).data('gopres_lang');
		var currentSwiperPres = $(swiperPres.activeSlide()).data('slideid');
		//alert(currentSwiperPres);
		var currentInnerSwiper = $(swiperInner.activeSlide()).data('slideid');
		//alert(currentInnerSwiper);
		localStorage[pre_prefix + 'current_swiper_pres'] = currentSwiperPres;
		localStorage[pre_prefix + 'current_inner_swiper'] = currentInnerSwiper;
		try {
			top.changePresentation(pres);
		} catch(e){
			alert('Go Pres: ' + pres);
			window.location.reload();
		}
	});
	
	$('body').hammer().on('tap', '[data-gopres]', function(){
		var pres = $(this).data('gopres');
		try {
			top.changePresentation(pres);
		} catch(e){
			alert('Go Pres: ' + pres);
			window.location.reload();
		}
	});
	
	$('body').hammer().on('tap', '[data-page]', function(){
		var pageid = $(this).data('page');
		swiperPage.swipeTo(pageid);
	});
	
	$('body').hammer().on('tap', '[data-videoswitch]', function(){
		var resource = $(this).data('videoswitch');
		var vid = '<video width="241" height="155" controls="controls"><source src="../5/'+resource+'" type="video/mp4" />Your browser does not support the video tag.</video>';
		$(".popup:visible .load-small-video").html(vid);
	});
	
	$('body').hammer().on('tap', '[data-scroll]', function(){
		var scrollid = $(this).data('scroll');
		swiperScroll.swipeTo(scrollid);
	});
	
	$('body').hammer().on('tap', '[data-go_detailaid]', function(){
		var gopage = $(this).data('go_detailaid');
		var position = gopage.split('_')[1];
		var activegroup = '_'+position;
		$('[data-active_group="'+activegroup+'"]').addClass('active').siblings().removeClass('active');
		if($(this).closest('.content').length && $('body').attr('data-activesection') == '_customcall'){
			alert('within call');
			var innerslideid = $(this).data('go_detailaid');
			go_section('customcall',this,innerslideid);
		} else {
			go_section('detailaid',this);
		}
	});
	
	$('body').hammer().on('tap', '[data-go_customcall]', function(){
		var gotoslidenum = $(this).attr('data-go_customcall');
		var gotosection = '_customcall';
		var innerslideindex = $('.swiper-slide[data-slideid="'+gotoslidenum+'"]').index();
		rightXLimit = null;
		swiperInner.swipeTo(innerslideindex,01);
	});
	
	$('body').hammer().on('tap', '.fav-thumb', function(){
		var gotoslidenum = $(this).attr('data-load');
		var gotosection = $(this).attr('data-section_id');
		var innerslideindex = $('.swiper-slide[data-slideid="'+gotoslidenum+'"]').index();
		goSlide(gotosection,gotoslidenum,true);
	});
	
	$('body').hammer().on('dragleft dragright', '.swipeable', function(){
		$(this).trigger('tap');
	});
	
	$('body').hammer().on('tap', '[data-go_slidecontent]', function(){
		var slidecontent = $(this).data('go_slidecontent');
		if(slidecontent == 'top'){
			openTopContent();
		} else {
			openBottomContent(true);
		}
	});
	
	$('body').hammer().on('pinch', '.content:not(.content-no-pinch)', function(e){
		e.gesture.stopDetect();
		$('#indicator').hide();
		if(!$(this).hasClass('home-page')){
			$(this).addClass('shrink');
			setTimeout(function(){
				var innerslideid = '_1_1_a';
				if($(swiperPres.activeSlide()).data('slideid') != '_detailaid'){
					goSlide('_detailaid',innerslideid);
				} else {
					var innerslideindex = $('.swiper-slide[data-slideid="'+innerslideid+'"]').index();
					rightXLimit = null;
					swiperInner.swipeTo(innerslideindex,01);
				}
			},500);
		}
	});
	
	$('body').hammer().on('tap', '.add-fav', function(e){
		e.gesture.stopDetect();
		var currsection = swiperPres.activeSlide().data('slideid');
		var currslide = swiperInner.activeSlide().data('slideid');
		var currslidetitle = swiperInner.activeSlide().data('analytics');
		if(currsection == '_customcall'){
			swal({ 
				type: "error",
				title: "Can't add favourite from custom call.", 
				allowOutsideClick: false,
				animation: false,
				//timer: 1000, 
				showConfirmButton: true 
			});
		} else {
			addToFavorites(currsection,currslide,currslidetitle);
		}
	});

	/*
$('body').hammer().on('tap', '[data-load]', function(e){
		if(!$(this).closest('.edit-state').hasClass('editing') && !$(e.target).hasClass('favstar')){
			var currentactivecall = $('[data-gocall].active');
			
			var data = $(this).data();
			var sectionid = data.section_id;
			var innerslideid = data.load;
			
			
			if(currentactivecall.length){
				
				currentactivecall = currentactivecall.attr('data-gocall');
				//alert(customBuild);
				//alert(currentactivecall);
				if(customBuild != currentactivecall){
					//goSlide(sectionid,innerslideid,true);
				} else {
					
				}
				
			} else {
				
				if(swiperPres.activeSlide().data('slideid') == sectionid && swiperInner.activeSlide().data('slideid') == innerslideid){
					closeBottomContent();
					closeSlideOverview();
				} else {
					
					if(swiperPres.activeSlide().data('slideid') != sectionid){
						goSlide(sectionid,innerslideid);
					} else {
						if(customBuild){
							goSlide(sectionid,innerslideid,true);
						} else {
							var innerslideindex = $('.swiper-slide[data-slideid="'+innerslideid+'"]:not(.swiper-slide-duplicate)').index() - 1;
							rightXLimit = null;
							swiperInner.swipeTo(innerslideindex,01);
						
						}
					}
				}
			}
		}
	});
*/
	
	$('body').hammer().on('hold', function(e){
		if ((!$('.tutorial-container').has(e.target).length > 0) && (!$(e.target).hasClass('tutorial-container')) && !$('.video-container').is(':visible') && e.target.tagName != "VIDEO"){
			closeBottomContent();
			closeTopContent();
			var posX = e.gesture.center.pageX;
			var posY = e.gesture.center.pageY;
			if(posX < 190){
				posX = 190;
			} else if(posX > 834){
				posX = 834;
			}
			if(posY < 77){
				posY = 77;
			} else if(posY > 691){
				posY = 691;
			}
			$('.pop-menu-block').css({
				'left':posX,
				'top':posY
			});
			$('body').addClass('pop-menu-open');
			$('.pop-menu').show(0, function(){
				hidePopups();
				hideVideo();
			});
		}
	});
	
	$('body').hammer().on('tap', '.pop-menu .block-content', function(e){
		$('body').removeClass('pop-menu-open');
		$('.pop-menu').hide();
	});
	
	$('body').hammer().on('tap', '.popup-container .block-content, .close-popups', function(e){
		if($('body').hasClass('popup-open')){
			hidePopups();
		}
	});
	
	$('body').hammer().on('tap', '.video-container .block-content', function(e){
		if($('body').hasClass('popup-open')){
			hideVideo();
		}
	});
	
	$('body').hammer().on('tap', '.card', function(){
		$(this).toggleClass('flipped');
	});
	
	$('body').hammer().on('tap', '[data-popup]', function(){
		var popid = $(this).data('popup');
		if($(this).parents('[data-popid]').length == 1){
			switchPopup(popid);
		} else {
			if(!$('body').hasClass('popup-opening')){
				showPopup(popid);
			}
		}
	});
	
	$('body').hammer().on('tap', '.reset-animation', function(){
		var parentContainer = $(this).parent();
		parentContainer.addClass('reset').removeClass('play');
		setTimeout(function(){
			parentContainer.removeClass('reset').addClass('play');
		}, 500);
	});
	
	$('body').hammer().on('tap', '.trigger-animation', function(){
		var parentContainer = $(this).parent();
		parentContainer.addClass('reset').removeClass('tap-to-trigger');
		setTimeout(function(){
			parentContainer.removeClass('reset').addClass('play');
		}, 500);
	});	
	
	$('body').hammer().on('tap', '[data-image_toggle]', function(){
		$(this).toggleClass('swapped');
	});
	
	$('body').hammer().on('tap', '[data-image_swap]', function(){
		var imageswapcontainer = $(this).closest('#image-swap-container');
		var image_swap = $(this).data('image_swap');
		if (imageswapcontainer.hasClass('swap-'+image_swap)){
			
		} else {
			imageswapcontainer.attr('class','swap-'+image_swap);
		}
	});
	
	$('body').hammer().on('tap', '[data-image_show]', function(){
		var image = $(this).data('image_show');
		$('[data-image="'+image+'"]').show();
	});
	
	$('body').hammer().on('tap', '[data-radio]', function(){
		var radio = $(this).data('radio');
		$(this).closest('[data-radio_active]').attr('data-radio_active',radio);
		$(this).find('.fa').attr('class','fa fa-check-square-o');
		$(this).siblings().find('.fa').attr('class','fa fa-square-o');
	});
	
	$('body').hammer().on('tap', '[data-swipeindicator]', function(e){
		var swipeindicator = $(this).data('swipeindicator');
		if(swipeindicator !== 'true'){
			touchIndicatorOn();
			$('[data-swipeindicator]').data('swipeindicator','true').addClass('active');
		} else {
			touchIndicatorOff();
			$('[data-swipeindicator]').data('swipeindicator','false').removeClass('active');
		}
	});
	
	$('body').hammer().on('tap', '.go-tutorial', function(){
		$('body').removeClass('pop-menu-open');
		$('.pop-menu').hide();
		$('.tutorial-container').show(0, function(){
			loadProcess = $.ajax({
				url: '_tutorial.html',
				success: function(content) {
					$('#tutorial-content').html(content);
					go_tut(1);
				}
			});
		});
	});
	
	$('#tutorial-content').hammer().on('tap', '[data-tut_id] .btn-warning', function(){
		var pop = $(this).closest('[data-tut_id]');
		var tutid = pop.data('tut_id');
		go_tut(tutid + 1);
	});
	
	$('#tutorial-content').hammer().on('tap', '[data-tut_id] .btn-default', function(){
		exit_tut();
	});
	
	$('body').hammer().on('tap', '.tabby a', function (e) {
  		e.preventDefault();
		$(this).tab('show');
	});
	
	$('body').on('shown.bs.tab', '#tabby-1 a', function(){
		$('.tab-pane .canvas').each(function(){
			var $canvas = $(this).find('canvas');
			if ($canvas.length){
				var context2 = $canvas[0].getContext('2d');
				context2.clearRect(0, 0, 600, 300);
				$canvas.remove();
			}
		}).promise().done(function(){
			var currcanvasdiv = $('.tab-pane.active .canvas');
			currcanvasdiv.append('<canvas width="600" height="300"></canvas>');
			var currcanvas = currcanvasdiv.children('canvas');
	      	var context = currcanvas[0].getContext('2d');
	      	//context.clearRect(0, 0, canvas.width, canvas.height);
	      	if(currcanvasdiv.hasClass('line')){
	      		//alert('line');
	      		new Chart(context).Line(lineChartData);
	      	} 
	      	if(currcanvasdiv.hasClass('pie')){
	      		//alert('pie');
	      		new Chart(context).Pie(pieData);
	      	}
			if(currcanvasdiv.hasClass('bar')){
				//alert('bar');
	      		new Chart(context).Bar(barChartData);
	      	}
	      	if(currcanvasdiv.hasClass('radar')){
	      		//alert('radar');
	      		new Chart(context).Radar(radarChartData);
	      	}
		});
	});
	
	$('body').on('shown.bs.tab', '#tabby-2 a', function(){
		var activetabpane = $('#productMonograph .tab-pane.active');
		var indexoftab = $(this).parent('li').index();
		activetabpane.html('<img src="../4/pm_'+(indexoftab+1)+'.jpg" alt="Product Monograph" />');
		if (swiperScroll1 != ''){
			swiperScroll1.swipeTo(0,0);
			setTimeout(function(){
				swiperScroll1.reInit();
			},200);
		}
	});
	
	$('body').hammer().on('tap', '.chartPrev', function(){
		var prev = $('#tabby-1 .active').prev().find('a');
		if (prev.length){
			prev.trigger('tap');
		} else {
			$('#tabby-1 li:last a').trigger('tap');
		}
	});
	$('body').hammer().on('tap', '.chartNext', function(){
		var next = $('#tabby-1 .active').next().find('a');
		if (next.length){
			next.trigger('tap');
		} else {
			$('#tabby-1 li:first a').trigger('tap');
		}
	});
	
	$('body').hammer().on('tap', '[data-goefficacy]', function(){
		var efficacy = $(this).data('goefficacy');
		var current = $('[data-efficacy]').attr('data-efficacy');
		if(efficacy != current){
			smallSwiper.swipeTo(0,0);
		    $('[data-efficacy]').attr('data-efficacy',efficacy);
			$('.small-swiper .canvas').toggleClass('hidden');
			initPieCharts();
		}
	});
	
	$('body').hammer().on('tap', '.small-slide-pagination span', function(){
		var indexspan = $(this).index();
		smallSwiper.swipeTo(indexspan);
	});
	
	$('body').hammer().on('tap', '.resource-container .email', function(){
		var emailresource = $(this).siblings('.resource').data('resource');
		//alert(emailresource);
		$('#sendto').val('');
		$('#sendto').val('');
		$('.selected-resource').html(emailresource);
		var resourcecat = $(this).closest('[data-resourcecat]').attr('data-resourcecat');
		$('#email-request-form').modal('show');
	});
	
	$('body').on('mouseup', '.btn-send-form:not(.disabled)', function(){
		var btn = $(this);
		btn.button('loading');
		$('input').blur();
		var resource = $('.selected-resource').html();
		var to = $('#sendto').val();
		var toname = $('#sendtoname').val();
		if(validateEmail(to)){
			//alert(projectID);
			//var message = $('#sendmessage').val();
			//var user = 'current-user';
			$.post('http://hifyre.com/projects/hifyredemo/send-request.php', {
				timestamp: new Date($.now()),
				id: projectID,
			    to: to,
			    toname: toname,
			    //user: user,
			    resource: resource
			    //message: message
			}, function(data){
				//alert('data: '+data);
				if(data.success){
					$('#email-request-form').modal('hide');
					swal({ 
						type: "success",
						title: data.success, 
						allowOutsideClick: false,
						animation: false,
						//timer: 1000, 
						showConfirmButton: true 
					});
				} else {
					swal({ 
						type: "error",
						title: data.error, 
						allowOutsideClick: false,
						animation: false,
						//timer: 1000, 
						showConfirmButton: true 
					});
				}
			}, 'json').done(function(){
				btn.button('reset');
			}).fail(function(x, s, e) {
				//save for later
				//alert(s);
				//alert(e);
				var requests = localStorage[prefix+'request_form'];
				requests = JSON.parse(requests);
				requests.push({
					timestamp: $.now(),
			        id: projectID,
				    to: to,
				    toname: toname,
				    //user: user,
				    resource: resource,
				    //message: message
			    });
				localStorage[prefix+'request_form'] = JSON.stringify(requests);
				$('#email-request-form').modal('hide');
				swal({ 
					type: "warning",
					title: "Request stored to be sent later.", 
					allowOutsideClick: false,
					animation: false,
					//timer: 1000, 
					showConfirmButton: true 
				});
				btn.button('reset');
			});
		} else {
			swal({ 
				type: "warning",
				title: "Please enter a valid email address.", 
				allowOutsideClick: false,
				animation: false,
				//timer: 1000, 
				showConfirmButton: true 
			});
			btn.button('reset');
		}
	});
	
	$('#email-request-form').on('hidden.bs.modal', function (e) {
		$('#sendto,#sendtoname,#sendmessage').val('');
		$('.selected-resource').html('');
		$('input').blur();
	})
	
	/* on page load, load pdf resources */
	displayResources();
	sendstoredrequests();
	
	
		
});

function sendstoredrequests(){
	var requests = localStorage[prefix+'request_form'];
	requests = JSON.parse(requests);
	//alert(requests);
	//alert(JSON.stringify(requests));
	if(requests.length){
		
		var newRequests = new Array();
		var x = 0;
		var loopArray = function(arr) {
		    customAlert(x,arr[x],function(){
		        x++;
		        if(x < arr.length) {
		            loopArray(arr);   
		        } else {
			        //alert('done');
			        localStorage[prefix+'request_form'] = JSON.stringify(newRequests);
		        }
		    }); 
		}
		
		function customAlert(i,request,callback) {
		    //console.log(request.to);
		    if(validateEmail(request.to)){
				$.post('http://hifyre.com/projects/hifyredemo/send-request.php', {
					timestamp: new Date(request.timestamp),
					id: request.id,
				    to: request.to,
				    toname: request.toname,
				    //user: request.user,
				    resource: request.resource,
				    //message: request.message
				}, function(data){
					if(data.success){
						//requests.splice(i, 1);
						//console.log('sent success: ' + request.toname );
						callback();
						//alert(data.success);
					} else {
						//requests.splice(i, 1);
						newRequests.push(request);
						//console.log('error sending: ' + request.toname);
						callback();
					}
				}, 'json').done(function(){
					
				}).fail(function() {
					//save for later
					//alert('Internet Connection Required. Request stored to be sent later.');
					newRequests.push(request);
					//console.log('error sending: ' + request.toname);
					callback();
				});
			} else {
				
			}
		}
		
		loopArray(requests);

	}
}

function go_tut(num){
	$('[data-tut_id]').hide();
	if(num == 2){
		openTopContent();
	} else if (num == 4){
		closeTopContent();
	} else if (num == 5){
		openBottomContent();
	} else if (num == 7){
		openBottomContent(true);
	} else if (num == 9){
		closeBottomContent();
	} else if (num == 11){
		exit_tut();
	}
	$('[data-tut_id="'+num+'"]').show();
}

function exit_tut(){
	closeBottomContent();
	closeTopContent();
	$('.tutorial-container').hide();
	$('body').addClass('pop-menu-open');
	$('.pop-menu').show();
}

function displayResources() {
	$.get('resources.xml', function(data) {
	    var parsed = $(data);
	    var template = $('#template');
	    parsed.find('ITEM').each(function(){
	    	var thisSection = template.clone().attr('id', '').removeClass('template');
            
            var category = $(this).find('CATEGORY').text();
            var resource = $(this).find('RESOURCE').text();
            var title = $(this).find('TITLE').text();
            var thumb = resource.substr(0, resource.lastIndexOf('.')) || resource;
            thumb += '-thumb.jpg';
            
            $(thisSection).attr('data-resourcecat',category);
            $(thisSection).attr('data-favid',category+'_'+resource);
            $('.resource', thisSection).attr('data-resource',resource);
            $('.resource img', thisSection).attr('src','../5/'+thumb);
            $('.resource p', thisSection).html(title);
            
            if($(this).attr('email') == 'true'){
	            $(thisSection).attr('data-emailfunction','true');
            }

            $('#resource-container').append(thisSection);
        });
	}).done(function() {

		//displaySlideOverview('detailaid','#slidelibrary-container .row');
		initSwipers();
		
	});
}




function displaySlideOverview(sectionid,appendSection,customcallnum) {
	//alert(sectionid);
	//alert(appendSection);
	//alert(customcallnum);
	$.get('slides.xml', function(data) {
	    var parsed = $(data);
	    var template = $('<div><div class="slide-thumb"><p></p><div class="slide-order"><span></span></div></div><span class="favstar"><i class="fa fa-star"></i></span></div>');

	    sectionid = sectionid.split(',');
	    $.each(sectionid, function(i,value){
	    	
	    	parsed.find(value.toUpperCase()).find('ITEM').each(function(ii,v){
		    	var thisSection = template.clone();
		        var id = $(this).find('ID').text();		        
		        var title = $(this).find('TITLE').text();
		        var numericid = id.replace('_','');
				numericid = numericid.replace('_','');
				numericid = numericid.replace('_','.');
				numericid = numericid.replace('a','1');
				numericid = numericid.replace('b','2');
				numericid = numericid.replace('c','3');
				numericid = numericid.replace('d','4');
				numericid = numericid.replace('e','5');
				
				var classattribute = $(this).attr('class');
				if(!classattribute){
					classattribute = '';
				}
	            
		
		        $('.slide-thumb',thisSection).attr('data-section_id','_'+value);
		        $('.slide-thumb',thisSection).attr('data-load',id);
		        $('.slide-thumb',thisSection).attr('data-originalslideorder',ii);
		        $('.slide-thumb',thisSection).attr('data-numericid',numericid);
		        
		        $('.slide-thumb',thisSection).attr('data-slide_title',title);
		        $('.slide-thumb',thisSection).css('background-image','url(../4/t'+id+'.png)');
		        $('p', thisSection).html(title);
		
	            $(appendSection).append(thisSection.wrap('<div class="col-xs-3 animated fadeIn '+classattribute+'"/>').parent());
	            
		        
		        
	    	});
    	
	        
	    });
	    
	}).done(function() {
		//displayFavSlides();
		if(customcallnum){
			if(customcallnum == 'empty'){
				$('.slide-thumb:not(.selected)').removeClass('chosen').closest('.col-xs-3').addClass('unchosen');
			} else {
				
			$.each(customcallnum, function(i,v){
				//console.log(v);
				$('.slide-thumb[data-load="'+v+'"]').addClass('selected').attr('data-slideorder',i).find('.slide-order span').html(i).closest('.col-xs-3').removeClass('unchosen');
			});
			$('.slide-thumb:not(.selected)').removeClass('chosen').closest('.col-xs-3').addClass('unchosen');
			$('.slide-thumb.selected').addClass('chosen').closest('.col-xs-3').removeClass('unchosen');
			$('.slide-thumb').each(function(){
				$(this).removeClass('selected').find('.slide-order span').html('');
			});
			var $wrapper = $('#slidelibrary-container > .row');
			$wrapper.addClass('invisible');
			setTimeout(function(){
			$wrapper.find('.col-xs-3').sort(function (a, b) {
			    return +$('.slide-thumb', a).attr('data-slideorder') - +$('.slide-thumb', b).attr('data-slideorder');
			}).appendTo( $wrapper );
			$wrapper.removeClass('invisible');
			}, 300);
			}
			
    	}
		
		var currentfavs = localStorage[prefix+'fav_slides'];
		currentfavs = JSON.parse(currentfavs);
		
		$.each(currentfavs, function(key, value){
			$('.swiper-slidelibrary [data-load="'+value.slideid+'"]').parent().addClass('fav-on');
		});	
		
		try {
			swiperSlidelibrary.reInit();
			swiperSlidelibrary.resizeFix(true);
			var activeSection = $('body').attr('data-activesection');
			var activeSlide = $('body').attr('data-activeslide');
			var position = activeSlide.indexOf("_", activeSlide.indexOf("_") + 1);
			var group = activeSlide.substr(0, position);
			if (activeSection == '_launch' || activeSection == '_advantage' || group == '_0' || group == '_m'){
				$('[data-slide_filter="_advantage"]').trigger('tap');
			} else {
				$('[data-slide_filter="'+group+'"]').trigger('tap');
			}
		} catch(e){
			
		}
		try {
			swiperCustomcalls.refresh();
			//swiperCustomcalls.resizeFix(true);
		} catch(e){
			
		}
	});
}



/* Initialize Swipers */
function initSwipers(){

	var currentSwiperPres = localStorage[pre_prefix + 'current_swiper_pres'];
	var currentInnerSwiper = localStorage[pre_prefix + 'current_inner_swiper'];
	var startslideindex;
	if(currentSwiperPres !== ''){
		$('.swiper-slide[data-slideid="'+currentSwiperPres+'"]').data('initial_slide',currentInnerSwiper);
		startslideindex = $('.swiper-slide[data-slideid="'+currentSwiperPres+'"]').index();
		localStorage[pre_prefix + 'current_swiper_pres'] = '';
		localStorage[pre_prefix + 'current_inner_swiper'] = '';
	} else {
		startslideindex = 0;
	}
	
	swiperPres = $('.swiper-pres').swiper({
		mode: 'horizontal',
		noSwiping: true,
		//initialSlide: startslideindex,
		queueEndCallbacks: true,
		onSlideChangeStart: function(swiper){
			closeBottomContent();
		},
		onSlideChangeEnd: function(swiper){
			loadslide();
		}
	});
	
	loadslide();
	
	swiperResources = $('.swiper-resources').swiper({
		mode: 'vertical',
		scrollContainer: true,
		scrollbar: {
			container :'.swiper-scrollbar-1',
			hide: false,
			draggable: false  
	    }
	});
	
	/*
swiperSlidelibrary = $('.swiper-slidelibrary').swiper({
		mode: 'vertical',
		scrollContainer: true,
		scrollbar: {
			container :'.swiper-scrollbar-2',
			hide: false,
			draggable: false  
	    }
	});
*/
	
	favsSwiper = $('.favs-swiper').swiper({
	    scrollContainer: true,
	    slidesPerView: 'auto',
	    scrollbar: {
			container :'.swiper-scrollbar-4',
			hide: false,
			draggable: false  
	    }
	});
	
	displayFavResources();
	displayFavSlides();
	swiperResources.swipeTo(0,0);
	swiperResources.reInit();
	swiperResources.resizeFix(true);
	//swiperSlidelibrary.reInit();
	//swiperSlidelibrary.resizeFix(true);
	favsSwiper.reInit();
}


/* Load Slides */
function loadslide() {
	var activeSlide = $(swiperPres.activeSlide());
	activeSlide.siblings().html('');
	var data = activeSlide.data();
	$('body').attr('data-activesection',data.slideid);
	//displaySlides(data.slideid);

	loadProcess = $.ajax({
		url: data.slideid+'.html',
		success: function(content) {
			activeSlide.html(content);
			
			//initInnerSwiper();
			displaySlides(data.slideid);
		}
	});
	
}


function displaySlides(sectionid) {
	
	$('.slide-pagination').html('');
	$.get('slides.xml', function(data) {
	    var parsed = $(data);
	    var template = $('<div class="swiper-slide swiper-no-swiping"></div>');
	    var tabtemplate = $('<a href="#" class="btn"><div class="navword"></div></a>');
	    $('.top-nav-bar a:not([data-go_slidecontent])').remove();
	    $('.top-nav-bar').addClass('unloaded');
	    
		if(currentCustomCall!== false){
			//console.log(currentCustomCall);
			$.each(currentCustomCall, function(i,v){
				//console.log(v);
				parsed.find(sectionid.substring(1).toUpperCase()).find('ID:contains('+v+'):first').each(function(){
			    	var thisSection = template.clone();
		            var parental = $(this).parent();
		            
		            var id = parental.find('ID').text();
		            var title = parental.find('TITLE').text();
		            
		            //var thumb = $(this).find('THUMB').text();
		            
		            $(thisSection).attr('data-slideid',id);
		            $(thisSection).attr('data-analytics',title);
		            
		            $('#'+sectionid+'_container').append(thisSection);
		            
				    var thisTab = tabtemplate.clone();
		            $(thisTab).attr('data-go'+sectionid,id);
		            var position = id.indexOf("_", id.indexOf("_") + 1);
					var activegroup = id.substr(0, position);
					$(thisTab).attr('data-active_group',activegroup);
					$('.navword',thisTab).html(title);
		            thisTab.insertBefore($('.top-nav-bar [data-go_slidecontent]'));
		            $('.top-nav-bar').removeClass('unloaded');
		           
		        });
			});
	        			
		} else {
			customBuild = false;
		    parsed.find(sectionid.substring(1).toUpperCase()).find('ITEM').each(function(){
		    	var thisSection = template.clone();
	            
	            var id = $(this).find('ID').text();
	            var title = $(this).find('TITLE').text();
	            
	            //var thumb = $(this).find('THUMB').text();
	            
	            $(thisSection).attr('data-slideid',id);
	            $(thisSection).attr('data-analytics',title);
	            
	            $('#'+sectionid+'_container').append(thisSection);
	            
	            
	            var thisTab = tabtemplate.clone();
	            $(thisTab).attr('data-go'+sectionid,id);
	            var position = id.indexOf("_", id.indexOf("_") + 1);
				var activegroup = id.substr(0, position);
				$(thisTab).attr('data-active_group',activegroup);
				$('.navword',thisTab).html(title);
	            thisTab.insertBefore($('.top-nav-bar [data-go_slidecontent]'));
	            $('.top-nav-bar').removeClass('unloaded');
	        });
        }
	}).done(function() {
		currentCustomCall = false;
		initInnerSwiper($(swiperPres.activeSlide()).data('initial_slide'));	
		initPopups('#common-pop');
	});
	/*
$.get('slides.xml', function(data) {
	    var parsed = $(data);
	    var template = $('<div class="swiper-slide swiper-no-swiping"></div>');
	    parsed.find(sectionid.substring(1).toUpperCase()).find('ITEM').each(function(){
	    	var thisSection = template.clone();
            
            var id = $(this).find('ID').text();
            var title = $(this).find('TITLE').text();
            //var thumb = $(this).find('THUMB').text();
            
            $(thisSection).attr('data-slideid',id);
            $(thisSection).attr('data-analytics',title);
            
            $('#'+sectionid+'_container').append(thisSection);
        });
	}).done(function() {
		initInnerSwiper($(swiperPres.activeSlide()).data('initial_slide'));	
		initPopups('#common-pop');
	});
*/
	
}



function initInnerSwiper(initial_slide){
	if($('.swiper-inner').length){
		var innerslideindex = $('.swiper-slide[data-slideid="'+initial_slide+'"]').index();
		if(innerslideindex == -1){
			innerslideindex = 0;
		}
		
		//$('.swiper-inner > .swiper-wrapper, .swiper-inner > .swiper-wrapper > .swiper-slide').removeClass('swiper-no-swiping');
		
		swiperInner = $('.swiper-inner').swiper({
			mode: 'horizontal',
			noSwiping: false,
			resistance: '100%',
			initialSlide: innerslideindex,
			//onSetWrapperTransform: preventNextPage,
			onSlideChangeStart: function(swiper){
				closeBottomContent();
			},
			onSlideChangeEnd: function(swiper){
				
				loadslideinner();
			},
			onFirstInit: function(swiper){
				
			}
		});
		loadslideinner();
	}
}

var preventNextPage = function (swiper, transform) {
    if (rightXLimit > transform.x) {
        swiper.setWrapperTranslate(rightXLimit);
    }
};

var resetRightLimit = function (swiper) {
    rightXLimit = swiper.getWrapperTranslate('x');
};

function loadslideinner() {
	var activeSlide = $(swiperInner.activeSlide());
	activeSlide.siblings().html('');
	$('.fake-product').remove();
	var data = activeSlide.data();
	$('body').attr('data-activeslide',data.slideid);
	
	var gopage = data.slideid;
	//alert(gopage);
	var position = gopage.split('_')[1];
	var activegroup = '_' + position;
	//alert(position);
	$('[data-active_group="'+activegroup+'"]').addClass('active').siblings().removeClass('active');
	
	loadProcess = $.ajax({
		url: data.slideid+'.html',
		success: function(content) {
			activeSlide.html(content);
			
			showFirstTab('#tabby-1');
			showFirstTab('#tabby-2');
			initSpin360('.product-1',true);
			initSlider('.vertical-slider');
			initSmallSwiper();
			
			analytics(activeSlide.data('analytics'));
			
			initPageSwiper();
			
			$(swiperPres.activeSlide()).data('initial_slide','');
			initPopups('.swiper-pres');
						
		}
	});
	
}



function initPageSwiper(initial_slide){
	if($('.swiper-page').length){
		swiperPage = $('.swiper-page').swiper({
			mode: 'vertical',
			noSwiping: true,
			onSlideChangeStart: function(swiper){
				$('[data-page="'+swiperPage.activeIndex+'"]').addClass('disabled').siblings().removeClass('disabled');
			},
			onSlideChangeEnd: function(swiper){
				
			}
		});
	}
}




function goSlide(slideid,innerslideid,forcereload){
	//alert(slideid);
	var slideindex = $('.swiper-slide[data-slideid="'+slideid+'"]').index();
	$('.swiper-slide[data-slideid="'+slideid+'"]').data('initial_slide',innerslideid);
	rightXLimit = null;
	if(slideid == $('body').attr('data-activesection')){
		closeBottomContent();
		//closeSlideOverview();
		loadslide();
	} else {
		swiperPres.swipeTo(slideindex,10);
	}
}

function go_section(section,that,innerslideid){
	//alert(section);
	//closeSlideOverview();
	if(!innerslideid){
		innerslideid = $(that).data('go_'+section);
	}
	
	if($(swiperPres.activeSlide()).data('slideid') != '_'+section){
		goSlide('_'+section,innerslideid);
	} else {
		//alert();
		var innerslideindex = $('.swiper-slide[data-slideid="'+innerslideid+'"]:not(.swiper-slide-duplicate)').index();
		//alert(innerslideindex);
		rightXLimit = null;
		setTimeout(function(){
			swiperInner.swipeTo(innerslideindex,01);
		}, 10);
	}
		
}






/* Favourite Slides */

function addToFavorites(currsection,currslide,currslidetitle) {
	//console.log(currsection);
	//console.log(currslide);
	//console.log(currslidetitle);
	
	/*
var currsection = swiperPres.activeSlide().data('slideid');
	var currslide = swiperInner.activeSlide().data('slideid');
	var currslidetitle = swiperInner.activeSlide().data('analytics');
*/
	
	var currentfavs = localStorage[prefix+'fav_slides'];
	currentfavs = JSON.parse(localStorage[prefix+'fav_slides']);
	
	var addnew = true;
	
	$.each(currentfavs, function(key,value) {
		if(value.sectionid == currsection && value.slideid == currslide){
			addnew = false;
			swal({ 
				type: "warning",
				title: "Already Added!", 
				allowOutsideClick: false,
				animation: false,
				timer: 1000, 
				showConfirmButton: false 
			});
			//$('#reaction-alert').removeClass('negative positive deleted').addClass('negative');
			//setTimeout(function(){
			//	$('#reaction-alert').removeClass('negative');
			//},1000);
		}
	});
	
	if (addnew == true || currentfavs == ''){
	
		currentfavs.push({
	        sectionid: currsection,
	        slideid: currslide,
	        slidetitle: currslidetitle
	    });
	    
	    localStorage[prefix+'fav_slides'] = JSON.stringify(currentfavs);
		
		var newSlide = favsSwiper.createSlide('<div class="fav-on"><div class="fav-thumb" data-section_id="'+currsection+'" data-load="'+currslide+'" data-slide_title="'+currslidetitle+'" style="background-image:url(../4/t'+currslide+'.png);"><p>'+currslidetitle+'</p></div><span class="favstar"><i class="fa fa-star"></i></span></div>');

		favsSwiper.appendSlide(newSlide);
		
		$('.slide-thumb[data-section_id="'+currsection+'"][data-load="'+currslide+'"]').parent().addClass('fav-on');
		
		swal({ 
			type: "success",
			title: "Favourite Added!", 
			allowOutsideClick: false,
			animation: false,
			timer: 1000, 
			showConfirmButton: false 
		});
		/*
$('#reaction-alert').removeClass('negative positive deleted').addClass('positive');
		setTimeout(function(){
			$('#reaction-alert').removeClass('positive');
		},1000);
*/
	}
		
}

function removeFromFavorites(currsection,currslideid,currslidetitle) {
	
	/*var currsection = $(that).data('section_id');
	var currslideid = $(that).data('load');
	var currslidetitle = $(that).data('slide_title');*/
	
	var currentfavs = JSON.parse(localStorage[prefix+'fav_slides']);
    var tempfavs = [];
    $.each(currentfavs, function(key,value) {
		if(value.slideid != currslideid){
			tempfavs.push({
		        sectionid: value.sectionid,
		        slideid: value.slideid,
		        slidetitle: value.slidetitle
		    });
		}
	});
	
	swal({ 
		type: "success",
		title: "Favourite Removed!", 
		allowOutsideClick: false,
		animation: false,
		timer: 1000, 
		showConfirmButton: false 
	});
	/*
$('#reaction-alert').removeClass('negative positive deleted').addClass('deleted');
	setTimeout(function(){
		$('#reaction-alert').removeClass('deleted');
	},1000);
*/
	
	localStorage[prefix+'fav_slides'] = JSON.stringify(tempfavs);
	$('.slide-thumb[data-section_id="'+currsection+'"][data-load="'+currslideid+'"]').parent().removeClass('fav-on');
	var favindex = $('.fav-thumb[data-section_id="'+currsection+'"][data-load="'+currslideid+'"]').closest('.swiper-slide');
	favindex.fadeOut(function(){
		favsSwiper.removeSlide(favindex.index());
		favsSwiper.swipeTo(0);
	});
	
}

function displayFavSlides(){

	var currentfavs = localStorage[prefix+'fav_slides'];
	currentfavs = JSON.parse(currentfavs);
	
	$.each(currentfavs, function(key, value){
		var newSlide = favsSwiper.createSlide('<div class="fav-on"><div class="fav-thumb" data-section_id="'+value.sectionid+'" data-load="'+value.slideid+'" data-slide_title="'+value.slidetitle+'" style="background-image:url(../4/t'+value.slideid+'.png);"><p>'+value.slidetitle+'</p></div><span class="favstar"><i class="fa fa-star"></i></span></div>');
		favsSwiper.appendSlide(newSlide);
		favsSwiper.reInit();
		$('[data-section_id="'+value.sectionid+'"][data-load="'+value.slideid+'"]').parent().addClass('fav-on');
	});	
	
}



/* Favourite Resources */
function displayFavResources(){
	var y = localStorage[prefix+'fav_resources'];
	y = JSON.parse(y);
	$.each(y, function(key, value){
		$('[data-favid="'+value+'"]').addClass('fav-on');
	});
	if (y.length){
		$('[data-resource_filter="favs"]').trigger('tap');
	} else {
		$('[data-resource_filter="all"]').trigger('tap');
	}
	$('[data-resource_filter="favs"] .badge').html(y.length);
}

function addFavResource(checkFav) {
	var y = localStorage[prefix+'fav_resources'];
	y = JSON.parse(y);
	var found = $.inArray(checkFav, y);
	if (found >= 0) {
	
	} else {
	    y.push(checkFav);
	}
	localStorage[prefix+'fav_resources'] = JSON.stringify(y);
}

function removeFavResource(checkFav) {
	var y = localStorage[prefix+'fav_resources'];
	y = JSON.parse(y);
	var removeItem = checkFav;
	y = $.grep(y, function(value) {
		return value != removeItem;
	});
	localStorage[prefix+'fav_resources'] = JSON.stringify(y);
}






function initPopups(container){
	if(container == '.swiper-pres'){
		$('#load-pop').html('');
	}
	$('[data-popid]', container).each(function(){
		$(this).append('<div class="close-popups btn"><i class="fa fa-times"></i></div>');
		var width = $(this).data('width');
		var height = $(this).data('height');
		$(this).css({
			'width':width,
			'height':height,
			'margin-top':((height/2)*-1)-10,
			'margin-left':(width/2)*-1
		});
		var bgsize = width + 'px ' + height + 'px';
		$(this).children('.popup-animated-piece').css({
			'background-size':bgsize
		});
		if(container == '.swiper-pres'){
			$(this).appendTo($('#load-pop'));
		}
	});
	$('.animation-container').each(function(){
		var width = $(this).width();
		var height = $(this).height();
		var bgsize = width + 'px ' + height + 'px';
		$(this).children('.popup-animated-piece').css({
			'background-size':bgsize
		});
		if(!$(this).hasClass('tap-to-trigger')){
			var parentContainer = $(this);
			parentContainer.addClass('reset').removeClass('play');
			setTimeout(function(){
				parentContainer.removeClass('reset').addClass('play');
			}, 500);
		}
	});
}


function showPopup(popid){
	$('body').addClass('popup-opening');
	$('.popup-container').removeClass('fadeOut').addClass('fadeIn').show();
	$('[data-popid="'+popid+'"]').show().addClass('current-pop');
	//$(swiperInner.activeSlide()).find('.content').removeClass('fadeIn').addClass('fadeOut');
	setTimeout(function(){
		$('body').addClass('popup-open').removeClass('popup-opening');
	},500);
}

function switchPopup(popid){
	$('.current-pop').removeClass('current-pop').hide();
	$('[data-popid="'+popid+'"]').show().addClass('current-pop');
}

function hidePopups(){
	$('.popup-container').removeClass('fadeIn').addClass('fadeOut');
	var myVideo = document.getElementsByTagName("video")[0];
	if(myVideo){
		myVideo.pause();
		$('.load-small-video').html('');
	}
	//$(swiperInner.activeSlide()).find('.content').removeClass('fadeOut').addClass('fadeIn');
	setTimeout(function(){
		$('.popup-container').hide(0, function(){
			$('body').removeClass('popup-open');
			$('.popup').hide().removeClass('current-pop');
		});
	},500);
}

function showVideo(source){
	$('.video-container').removeClass('fadeOut').addClass('fadeIn').show();
	var vid = '<video width="640" height="360" controls="controls"><source src="../5/'+source+'" type="video/mp4" />Your browser does not support the video tag.</video>';
    $("#load-video").html(vid);
    var myVideo = document.getElementsByTagName("video")[0];
	if(myVideo){
		myVideo.play();
	}
    
	$(swiperInner.activeSlide()).find('.content').removeClass('fadeIn').addClass('fadeOut');
	setTimeout(function(){
		$('body').addClass('popup-open');
		
	},500);
}

function hideVideo(){
	$('.video-container').removeClass('fadeIn').addClass('fadeOut');
	var myVideo = document.getElementsByTagName("video")[0];
	if(myVideo){
		myVideo.pause();
		$('#load-video').html('');
	}
	$(swiperInner.activeSlide()).find('.content').removeClass('fadeOut').addClass('fadeIn');
	setTimeout(function(){
		$('.video-container').hide(0, function(){
			$('body').removeClass('popup-open');
		});
	},500);
}


function closeBottomContent(){
	closeSlideOverview();
	$('.content-cover-2').removeClass('fadeIn').addClass('fadeOut');
	$('body').removeClass('open-slide-content-bottom open-full');
	setTimeout(function(){
		$('.content-cover-2').hide();
	}, 500);
}

function closeTopContent(){
	$('.content-cover-1').removeClass('fadeIn').addClass('fadeOut');
	$('body').removeClass('open-slide-content-top');
	setTimeout(function(){
		$('.content-cover-1').hide();
	}, 500);
}

function openBottomContent(full){
	$('.content-cover-2').removeClass('fadeOut').addClass('fadeIn').show();
	if(full){
		setTimeout(function(){
			loadCustomCalls();
		}, 500);
		
		$('body').addClass('open-slide-content-bottom open-full');
	} else {
		favsSwiper.swipeTo(0,0);
		//swiperSlidelibrary.swipeTo(0,0);
		//swiperSlidelibrary.resizeFix(true);
		$('body').addClass('open-slide-content-bottom');
		
	}
}

function openTopContent(){
	swiperResources.swipeTo(0,0);
	swiperResources.resizeFix(true);
	$('.content-cover-1').removeClass('fadeOut').addClass('fadeIn').show();
	$('body').addClass('open-slide-content-top');
}


function goResource(resource){
	var resourceType = checkFileType(resource);
	if (resourceType == 'pdf') {
		//alert('Open PDF: '+resource);
		try {
			window.location.href = '../5/'+resource;
			//top.navigateScene(projectID, resource);
		} catch (e){
			alert('Open PDF: '+resource);
		}
	} else if (resourceType == 'video'){
		showVideo(resource);
	} else {
		alert('Unable to open resource. Invalid file type: '+resourceType);
	}
}


/* FILE TYPE */
function checkFileType(file) {
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    extension = extension.toLowerCase();
    switch(extension) {
        case 'jpg':
        case 'png':
        case 'gif':
            return 'image';
        break;
        case 'css':
        case 'txt':
        case 'html':
        case 'htm':
        case 'js':
        	return 'text';
        break;
        case 'mp4':
            return 'video';
        break;
        case 'mp3':
            return 'sound';
        break;
        case 'pdf':
            return 'pdf';
        break;
        case 'zip':
        case 'rar':
            return 'compressed';
        break;
        default:
            return 'unknown';
    }
};



function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

function tm(unix_tm) {
    var dt = new Date(unix_tm*1000);
    document.writeln(dt.getHours() + '/' + dt.getMinutes() + '/' + dt.getSeconds() + ' -- ' + dt + '<br>');

}

function touchIndicatorOn() {
	$('body').append('<div id="indicator"></div>');
	var endCoords = {};
	$('html').hammer().on('touch drag', function(e) {
		$('#indicator').show().css({top:e.gesture.center.pageY, left:e.gesture.center.pageX});
	});
	$('html').hammer().on('release', function(e) {
		$('#indicator').hide();
	});
}

function touchIndicatorOff() {
	$('html').hammer().off('touch drag release');
	$('#indicator').remove();
}


function analytics(title) {
	var analyticsTitle = projectID+'/'+title;
	try {
    	//alert(analyticsTitle);
    	top.virtualScene(analyticsTitle);
	} catch(err){
		console.log(analyticsTitle);
    	//none
    }
}





/****** Custom Content Functions *****/
function showFirstTab(tabid){
	var tabby = $(tabid);
	if (tabby.length){
		tabby.find('a:first').tab('show');
	}
}

function initSpin360(productid,autospin){
	if(spin){
	    spin.spritespin('destroy');
	    api = null;
	    if(spin2){
		    spin2.spritespin('destroy');
		    api2 = null;
	    }
    }
    
	if($(productid).length){
		spin = $('#mySpriteSpin');
		var loadednum = 0;
		$("#mySpriteSpin").spritespin({
		    source: SpriteSpin.sourceArray('../4/360/{lane}_{frame}.png', {
				lane: [1,3],
				frame: [1,36],
				digits: 1
			}),
			frames: 36,
			lanes: 3,
			frame: 36,
			sense: 2,
			senseLane: -5,
		    behavior: 'drag',
		    width   : 640,
		    height  : 480,
		    frameTime : 60,
		    animate: autospin,
		    loop: false,
		    //wrap: false,
		    reverse: false,
		    stopFrame: 10,
		    onFrame : function(){},
		    onInit : function(){
			    
		    },
		    onProgress : function(){
		    	loadednum++;
		    	var loadedpercent = parseFloat(api.data.frames/loadednum*100);
			  	$('.spinner span').html(loadedpercent + '%');
		    },
		    onLoad : function(){
			    var clonedProduct = $('#mySpriteSpin').clone(true,true);
		        $(clonedProduct).addClass('fake-product');
				$('.top-nav-bar').before(clonedProduct);
				$('.spinner').remove();
		    }
		  });
		  api = spin.spritespin("api");
	} else {
		
	}
}

function initSlider(sliderid){
	if(slidervertical){
		$(sliderid).slider("destroy");
	}
	if($(sliderid).length){
	    var slidervertical = $(sliderid).slider({
		    orientation: "vertical",
		    min: 0,
		    max: 3,
		    value: 2,
		    slide: function( event, ui ){
			    event.stopPropagation();
			    slidervertical.slider('value', closest(ui.value,values2));
			    return false;
		    },
		    change: function( event, ui ){
		    	$('[data-history-vertical-slider]').attr('data-history-vertical-slider',ui.value);
		    }
	    })
    }
}

function productMonograph(){
	if ($('.swiper-scroll-monograph').length){
		swiperScroll1 = $('.swiper-scroll-monograph').swiper({
			mode: 'vertical',
			scrollContainer: true,
			slidesPerView: 1
		});
	} else {
		if (swiperScroll1 != ''){
			swiperScroll1.destroy();
			swiperScroll1 = '';
		}
	}
	
}

function initSmallSwiper(){
	
    if($('.small-swiper').length){
	    
	    var smallswiperswiping = true;
	    var smallpagination = false;
	    if($('#time-dragger').length){
		    smallswiperswiping = false;
	    }
	    if($('.small-slide-pagination').length){
		    smallpagination = '.small-slide-pagination';
	    }
	    smallSwiper = $('.small-swiper').swiper({
	    	mode: 'horizontal',
			noSwiping: smallswiperswiping,
			resistance: '100%',
			pagination: smallpagination,
			onSlideChangeEnd: function(){
				$('[data-doubleresource]').each(function(){
					var resources = $(this).attr('data-doubleresource');
					$(this).removeAttr('data-doubleresource').html('').attr('data-resource',resources);
				});
				initPieCharts();
			},
			onFirstInit: function(){
				setTimeout(function(){
					initPieCharts();
				}, 500);
			}
	    });
	    
    } else if(smallSwiper){
	    //smallSwiper.destroy();
    }
}

function initPieCharts(){
	$('.small-swiper .canvas').each(function(){
		var $canvas = $(this).find('canvas');
		if ($canvas.length){
			console.log($canvas);
			var context2 = $canvas[0].getContext('2d');
			context2.clearRect(0, 0, 200, 200);
			$canvas.remove();
		}
	}).promise().done(function(){
		var currcanvasdiv = $('.small-swiper .swiper-slide-active .canvas:not(.hidden)');
		if(currcanvasdiv.length){
			currcanvasdiv.each(function(){
				$(this).append('<canvas width="200" height="200"></canvas>');
				var currcanvas = $(this).children('canvas');
		      	var context = currcanvas[0].getContext('2d');
		      	
		      	if($(this).hasClass('canvas-97')){
		      		//alert('pie');
		      		new Chart(context).Pie(pieData97);
		      	}
		      	if($(this).hasClass('canvas-100')){
		      		//alert('pie');
		      		new Chart(context).Pie(pieData100);
		      	}
		      	if($(this).hasClass('canvas-75')){
		      		//alert('pie');
		      		new Chart(context).Pie(pieData75);
		      	}
				
			});
		}
	});
}

/****** CHARTS ******/
var lineChartData = {
	labels : ["January","February","March","April","May","June","July"],
	datasets : [
		{
			fillColor : "rgba(21,116,186,0.8)",
			strokeColor : "rgba(21,116,186,1)",
			pointColor : "rgba(21,116,186,1)",
			pointStrokeColor : "#fff",
			data : [65,59,90,81,56,55,40]
		},
		{
			fillColor : "rgba(221,153,51,0.8)",
			strokeColor : "rgba(221,153,51,1)",
			pointColor : "rgba(221,153,51,1)",
			pointStrokeColor : "#fff",
			data : [28,48,40,19,96,27,100]
		}
	]
}
	
var barChartData = {
	labels : ["January","February","March","April","May","June","July"],
	datasets : [
		{
			fillColor : "rgba(21,116,186,0.8)",
			strokeColor : "rgba(21,116,186,1)",
			data : [65,59,90,81,56,55,40]
		},
		{
			fillColor : "rgba(221,153,51,0.8)",
			strokeColor : "rgba(221,153,51,1)",
			data : [28,48,40,19,96,27,100]
		}
	]
}
	
var pieData = [
	{
		value: 50,
		color:"#1574ba"
	},
	{
		value : 100,
		color : "#f89838"
	},
	{
		value : 20,
		color : "#dedad9"
	}
];

var pieData97 = [
	{
		value: 97,
		color:"#f89838"
	},
	{
		value : 3,
		color : "#dedad9"
	}
];

var pieData100 = [
	{
		value: 100,
		color:"#f89838"
	}
];

var pieData75 = [
	{
		value: 75,
		color:"#f89838"
	},
	{
		value : 25,
		color : "#dedad9"
	}
];
		
var radarChartData = {
	labels : ["Eating","Drinking","Sleeping","Designing","Coding","Partying","Running"],
	datasets : [
		{
			fillColor : "rgba(21,116,186,0.8)",
			strokeColor : "rgba(21,116,186,1)",
			pointColor : "rgba(21,116,186,1)",
			pointStrokeColor : "#fff",
			data : [65,59,90,81,56,55,40]
		},
		{
			fillColor : "rgba(221,153,51,0.8)",
			strokeColor : "rgba(221,153,51,1)",
			pointColor : "rgba(221,153,51,1)",
			pointStrokeColor : "#fff",
			data : [28,48,40,19,96,27,100]
		}
	]
}



